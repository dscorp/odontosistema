import { AuthGuard } from './core/guards/auth.guard';
import { RouterModule, Routes } from "@angular/router";
import { BodyComponent } from "./core/components/body/body.component";
import { LoginComponent } from "./core/components/login/login.component";
import { RegistrarcitaComponent } from './featured/cita/registrarcita/registrarcita.component';

const routes: Routes = [

  {
    path: '',
    component: LoginComponent,
  },

  {
    path: 'dental',
    component:BodyComponent,
    canActivate: [AuthGuard],
    loadChildren: () => import('./core/core.module').then(m => m.CoreModule)
  },




]

export const APP_ROUTES = RouterModule.forRoot(routes);
