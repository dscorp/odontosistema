import { Consulta } from 'src/app/shared/model/consulta';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import { Component, OnInit } from '@angular/core';
import { Route, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contenedorconsulta',
  templateUrl: './contenedorconsulta.component.html',
  styleUrls: ['./contenedorconsulta.component.css']
})
export class ContenedorconsultaComponent implements OnInit {
  idconsulta:string
  nombrePaciente: any;

  flagLoading=false;

  constructor(
    private router:Router,
    private route: ActivatedRoute,
    public consultaService:ConsultaService,
    ) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
       this.idconsulta = params.get('id');
       this.nombrePaciente = params.get('nombre')
       this.obtenerConsultaActual()
   });
  }

  obtenerConsultaActual()
  {
    this.flagLoading=true;

    this.consultaService.getById(this.idconsulta).subscribe((consulta:Consulta) =>{
          this.consultaService.consultaDesdeFirebase=consulta;
          this.flagLoading=false;

    })

  }

  finish(){
    this.router.navigate(['dental/consultas/'])
  }


  navegar(vista:string)
  {

     this.router.navigate([vista])
  }

}
