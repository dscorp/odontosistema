import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { utils } from 'protractor';
import { ConsultaService } from 'src/app/shared/services/consulta.service';

@Component({
  selector: 'app-vercontenedorconsulta',
  templateUrl: './vercontenedorconsulta.component.html',
  styleUrls: ['./vercontenedorconsulta.component.css']
})
export class VercontenedorconsultaComponent implements OnInit {

  consultas: any[] = []

  exaArrayClinica : any[] = []

  constructor(
    private activateRoute: ActivatedRoute,
    private consultaService: ConsultaService
  ) { }

  ngOnInit(): void {
    const id  = this.activateRoute.snapshot.params.id
    console.log('quiza este bien')
    console.log(id)

    this.getConsulta(id);

    this.exaArray(id);
  }

  getConsulta(id:string){
    this.consultaService.getById(id).subscribe((data:any) => {

      this.consultas = data;
      this.consultas = Array.of(this.consultas)
      console.log('esa es')
      console.log(this.consultas);
    })
  }

  exaArray(id:string){
    this.consultaService.getById(id).subscribe((data : any) => {

      this.exaArrayClinica = data.examenClinico;
      this.exaArrayClinica = Array.of(this.exaArrayClinica)
      console.log('por sia aca')
      console.log(this.exaArrayClinica)
    })
  }

}
