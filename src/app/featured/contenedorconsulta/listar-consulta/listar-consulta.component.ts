import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Buscar } from 'src/app/shared/model/buscar';
import { ConsultaService } from 'src/app/shared/services/consulta.service';

@Component({
  selector: 'app-listar-consulta',
  templateUrl: './listar-consulta.component.html',
  styleUrls: ['./listar-consulta.component.css'],
})
export class ListarConsultaComponent implements OnInit {
  public buscar = new Buscar();
  consultas: any[] = [];
  displayedColumns = ['dni', 'nombre', 'motivo', 'opciones'];
  dataSource: MatTableDataSource<any>;

  dataAsArray = new Array<any>();

  busqueda: string[] = ['dni', 'nombres'];

  flagPaginado = true;
  flagLoading = false;
  flagBuscando = false;
  ultimoDocumento: any = null;
  flagNoMoreResults: boolean = false;

  constructor(
    private consultaService: ConsultaService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.listarConsultas(10);
  }

  goGestion(consulta: any) {
    this.router.navigate([
      'dental/contenedorconsulta/' +
        consulta.id +
        '/' +
        consulta.nombres +
        ' ' +
        consulta.apellidos,
    ]);
  }

  goTratamiento(consulta: any) {
    this.router.navigate(['dental/odontograma/', consulta.id]);
  }

  navigate(consulta: any) {
    // this.router.navigate(['dental/pagoconsulta'], { queryParams: { idConsulta: idconsulta, dni:dni } });
    console.log(consulta);
    this.consultaService.selectedConsulta = consulta;
    console.log('holi');
    console.log(this.consultaService.selectedConsulta);
    this.router.navigate(['dental/pagoconsulta']);
  }

  navigate2(consulta2: any) {
    this.consultaService.selectedConsulta = consulta2;
    this.router.navigate(['dental/consulta/' + consulta2.id]);
    console.log(this.consultaService.selectedConsulta.id);
  }

  covertirdatadirestoreArray(data: any) {
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  covertirdatosparabusqueda(data: any) {
    this.dataAsArray = new Array<any>();
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }




  searchdni(dni: string) {
    if (dni.length == 8) {
      this.consultaService.search(dni).subscribe((data: any) => {
        let items = this.covertirdatosparabusqueda(data);
        this.dataSource = new MatTableDataSource(items);
      });
    } else if (dni.length == 0) {


        this.flagPaginado = true;
        this. flagLoading = false;
        this. flagBuscando = false;
        this.ultimoDocumento= null;
        this.flagNoMoreResults= false;

       this.listainicial()
      }

  }

  listarConsultas(registrosAMostrar) {
    this.flagLoading = true;

    this.consultaService
      .paginarsiguientes(this.ultimoDocumento, registrosAMostrar)
      .subscribe(
        (data: any) => {
          this.ultimoDocumento = data.docs[data.docs.length - 1];
          let items = this.covertirdatadirestoreArray(data);
          this.dataSource = new MatTableDataSource(items);
          this.flagLoading = false;

          if (data.docs.length < registrosAMostrar) {
            this.flagLoading = false;
            this.flagNoMoreResults = true;
          }
        },
        (err) => {
          this.flagLoading = false;
        }
      );
  }

  listainicial() {

    this.consultaService
    .paginarsiguientes(null, 10)
    .subscribe(
      (data: any) => {
        this.dataAsArray = new Array<any>();
        this.dataSource = new MatTableDataSource(null);
        this.ultimoDocumento = data.docs[data.docs.length - 1];
        let items = this.covertirdatadirestoreArray(data);
        this.dataSource = new MatTableDataSource(items);
      })

  }

  scrollHandler(e) {
    switch (e) {
      case 'top':
        break;

      case 'bottom':
        if (this.flagNoMoreResults == false) {
          this.listarConsultas(10);
        }

        break;
    }
  }

  // listarConsultas(){
  //   this.listarConsultaService.read().subscribe((data: any) => {
  //     this.dataSource = data.map(item => {
  //       return {
  //         id: item.payload.doc.id,
  //         ...item.payload.doc.data() as any
  //       } as MatTableDataSource<any>
  //     })
  //     console.log('esto es')
  //     console.log(this.dataSource )
  //   })
  // }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  // }

  applyFilter(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }
}
