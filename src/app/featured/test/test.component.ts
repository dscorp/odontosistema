import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

/*

  form4: FormGroup;
  
    this.form4 = this._formBuilder.group({
      nombre_generico_medicamento: ['', Validators.required],
      cuidado: ['', Validators.required],
      dosis: ['', Validators.required],
      examenes_auxiliares: ['', Validators.required],
      via: ['', Validators.required],
      referencia: ['', Validators.required],
      tiempo: ['', Validators.required],
    });
    


  pform4(){
    console.log("Formulario 04");
    console.log(this.form4.value);
  }
*/
