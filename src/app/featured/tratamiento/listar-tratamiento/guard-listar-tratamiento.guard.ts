import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import {
  CanActivate
} from '@angular/router';
import { TratamientoService } from 'src/app/shared/services/tratamiento.service';

@Injectable({
  providedIn: 'root',
})
export class GuardListarTratamiento implements CanActivate {
  constructor(
    private tratamientoService: TratamientoService,
    private locate: Location
  ) {}

  canActivate(): boolean {
    console.log('guard',this.tratamientoService.tratamientoSelected);
    if (this.tratamientoService.tratamientoSelected != undefined) {
      return true;
    } else {
      return false;
    }
  }
}
