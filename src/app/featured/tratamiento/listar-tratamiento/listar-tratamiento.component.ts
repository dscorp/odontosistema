import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Buscar } from 'src/app/shared/model/buscar';
import { Tratamiento } from 'src/app/shared/model/tratamiento';
import { TratamientoService } from 'src/app/shared/services/tratamiento.service';

@Component({
  selector: 'app-listar-tratamiento',
  templateUrl: './listar-tratamiento.component.html',
  styleUrls: ['./listar-tratamiento.component.css']
})
export class ListarTratamientoComponent implements OnInit {

  public buscar = new Buscar();
  tratamientos : any[] =[];
  displayedColumns = ['dni', 'nombre', 'fecha', 'tratamiento', 'opciones']
  dataSource: MatTableDataSource<any>

  dataAsArray = new Array<any>();

  flagPaginado = true;
  flagLoading = false;

  ultimoDocumento: any = null;

  constructor(
    private tratamientoService: TratamientoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.listarTratamientos(10);
  }
    covertirdatadirestoreArray(data: any) {
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  covertirdatosparabusqueda(data: any) {
    this.dataAsArray = new Array<any>();
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  searchdni(dni: string) {
    if (dni.length == 8) {
      this.tratamientoService.search(dni).subscribe((data: any) => {
        // this.dataSource =  new MatTableDataSource(data);
        // console.log(data)
        let items = this.covertirdatosparabusqueda(data);
        this.dataSource = new MatTableDataSource(items);
      });
    } else if (dni.length == 0) {
      this.dataAsArray = new Array<any>();
      this.ultimoDocumento = null;
      this.listarTratamientos(10);
    }
  }

  listarTratamientos(registrosAMostrar){
    if (this.flagPaginado) {
      this.flagLoading = true;
      this.tratamientoService
      .paginarsiguientes(this.ultimoDocumento, registrosAMostrar)
      .then((data:any) => {
        console.log('paginando');
        this.ultimoDocumento = data.docs[data.docs.length - 1];
        console.log('acaaaa')
        console.log(this.ultimoDocumento)
        this.tratamientos = this.covertirdatadirestoreArray(data);
       this.dataSource = new MatTableDataSource(this.tratamientos)
       console.log('ptmr')
       console.log(this.dataSource.data)
       this.flagLoading = false;
       if (data.docs.length < registrosAMostrar) {
         this.flagPaginado = false;
         console.log(' no hay mas datps');
         this.flagLoading = false;
       }
      })
    }
  }


  scrollHandler(e) {
    switch (e) {
      case 'top':
        break;

      case 'bottom':
        this.listarTratamientos(10);

        break;
    }
  }

  applyFilter(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }


  navigateToPagoTratamiento(inTratamientoSelected)
  {
    this.tratamientoService.tratamientoSelected=inTratamientoSelected

    this.router.navigate(['/dental/pagoplantratamiento'])
  }

 onClick(tratamientos: any){
   console.log(tratamientos)
  this.router.navigate(['/dental/tratamiento/', tratamientos.id])
 }





}
