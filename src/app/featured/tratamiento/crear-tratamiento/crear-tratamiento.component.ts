import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Pago } from 'src/app/shared/model/pago';
import { Sesion } from 'src/app/shared/model/sesion';
import { Tratamiento } from 'src/app/shared/model/tratamiento';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import { PagoService } from 'src/app/shared/services/pago.service';
import { TratamientoService } from 'src/app/shared/services/tratamiento.service';
import { Utils } from 'src/app/shared/utils/utlis';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-tratamiento',
  templateUrl: './crear-tratamiento.component.html',
  styleUrls: ['./crear-tratamiento.component.css'],
})
export class CrearTratamientoComponent implements OnInit {

  id : string | null;
  update: boolean;
  idtratamiento: string;
  sesion = new Sesion();
  tratamiento = new Tratamiento();

  servicios = [];

  constructor(
    private tratamientoService: TratamientoService,
    private consultaService: ConsultaService,
    private aRoute: ActivatedRoute,
    private router: Router,
    private pagoService: PagoService
  ) {
    this.servicios = Utils.servicios;
    this.id = this.aRoute.snapshot.paramMap.get('id');
    this.idtratamiento =  this.aRoute.snapshot.paramMap.get('idtratamiento');

    if(this.idtratamiento == null){
      this.update = false;
    }else{
      this.update = true;
    }

    console.log(this.update);

    if(this.update == false){
      this.getConsulta(this.id);
    }else{
      this.getTratamiento(this.idtratamiento)
    }

   }
  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  getTratamiento(id:string){
    this.tratamientoService.getById2(id).subscribe(data =>{
        this.tratamiento = data;
        console.log("get tratamiento");
        console.log(this.tratamiento);
    })
  }

  getConsulta(id:string){
    console.log("Get consulta by id")
    this.consultaService.getById(id).subscribe(data =>{
      console.log(data);
      this.tratamiento.id_consulta = this.id;
      this.tratamiento.dniPaciente = data['dni'];
      this.tratamiento.nombreCompleto =
        data['nombres'] + ' ' + data['apellidos'];
        this.tratamiento.telefonoPaciente=data['telefono']
      this.tratamiento.costoTotal = 0;
    });
    console.log(this.tratamiento);
  }

  push(form: NgForm) {
    if (form.valid) {
      this.tratamiento.sessiones.push(this.sesion);
      this.tratamiento.servicios.push(this.sesion.servicio)
      this.tratamiento.costoTotal += this.sesion.precio;
      this.sesion = new Sesion();
      form.resetForm();
    }
  }

  eliminar(index) {
    this.tratamiento.costoTotal -= this.tratamiento.sessiones[index].precio;
    this.tratamiento.sessiones.splice(index, 1);
  }

  save() {

    if(this.update == false){
      this.tratamientoService.save(this.tratamiento).then(data => {
        this.consultaService.update(this.tratamiento.id_consulta,{isWithTratamiento:true})
        var pago = new Pago();
        pago.monto = this.tratamiento.montoAbonado
        pago.fecha = new Date().getTime();
        pago.concepto = 'cuota inicial';
        pago.idPlanTratamiento = data.id;
        pago.idConsulta = this.tratamiento.id_consulta;
        pago.isPagoDeTratamiento = true;
        pago.dniPaciente = this.tratamiento.dniPaciente;
  
        this.pagoService.save(pago).then((response) => {
          Swal.fire({
            title:"Tratamiento registrado exitosamente",
            icon: 'success',
          }).then(function() {
            this.router.navigate(['dental/consultas']);
          }.bind(this));
        })
      }).catch(e => {
        Swal.fire({
          title:"Ocurrio un error al registrar el tratamiento",
          icon: 'error',
          text: e.message,
        })
      })
  
    }else{
      //Actualizar
  
      this.tratamientoService.updateSessiones(this.idtratamiento, this.tratamiento.costoTotal,this.tratamiento.sessiones).then(data => {
        Swal.fire({
          title:"Se actualizaron las sessiones exitosamente",
          icon: 'success',
        }).then(function() {
          this.router.navigate(['dental/listadoTratamientos/'])
        }.bind(this));
      }).catch(e => {
        console.log('error', e);
      })
    }
  

  }


}
