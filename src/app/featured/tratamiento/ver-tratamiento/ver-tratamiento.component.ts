import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Tratamiento } from 'src/app/shared/model/tratamiento';
import { TratamientoService } from 'src/app/shared/services/tratamiento.service';

@Component({
  selector: 'app-ver-tratamiento',
  templateUrl: './ver-tratamiento.component.html',
  styleUrls: ['./ver-tratamiento.component.css']
})
export class VerTratamientoComponent implements OnInit {

  tratamiento: any[] = []

  dataAsArray = new Array<any>();
  constructor(
    private tratamientoService: TratamientoService,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {

    const id= this.activeRoute.snapshot.params.id

    this.getTratamiento(id),

    console.log(id)
  }
  // covertirdatadirestoreArray(data:any){
  //   data.docs.map(doc => {
  //     this.dataAsArray.push({
  //       ...doc.data(),
  //       "id": doc.id
  //     })
  //   })
  //   return this.dataAsArray

  // }

  getTratamiento(id:string){
    this.tratamientoService.getById(id).subscribe(data => {

      // console.log(data.payload.data())

      this.tratamiento = data.payload.data();
      this.tratamiento =Array.of(this.tratamiento);
      console.log('nuevi')
      console.log(this.tratamiento)
      // this.tratamiento = []
      //  data.forEach((element:any) => {
      //   this.tratamiento.push({
      //     id: element.payload.doc.id,
      //     ...element.payload.doc.data()
      //   })
      // });
      // console.log('aca es ')
      // console.log(this.tratamiento)
      // console.log(data.payload.data())
      // this.tratamiento = data.payload.data()
      // console.log('nuevi')
      // console.log(this.tratamiento)
      // this.tratamiento = data
      // console.log('oajla')
      // console.log(this.tratamiento)
    },  
      err =>console.log(err),
      () => console.log('getTratamientos completed')
    );
    
  }

  // getTratamiento(id:string){
  //   this.tratamientoService.getById(id).pipe().subscribe((data:any) => { 
  //     this.tratamiento  = data
  //     console.log('me parece que aca es ')
  //     console.log(this.tratamiento)
  //   })
  // }

}
