import { Consulta } from 'src/app/shared/model/consulta';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ExamenclinicoService } from 'src/app/shared/services/examenclinico.service';
import { ConsultaService } from './../../shared/services/consulta.service';
@Component({
  selector: 'app-examenclinico',
  templateUrl: './examenclinico.component.html',
  styleUrls: ['./examenclinico.component.css']
})
export class ExamenclinicoComponent implements OnInit {

  exaClinico = new Array<any>();
  id: string | null;
  form: FormGroup;
  idconsulta: any;
  constructor(
    private fb: FormBuilder,
    public consultaService: ConsultaService,
    private location: Location,
    private route: ActivatedRoute,
    private examenClinicoService: ExamenclinicoService
  ) { this.id = this.route.snapshot.paramMap.get('id'); }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.idconsulta = params.get('id');

      this.setExamenClinico()
    });


    this.form = this.fb.group({
      pulso: [this.consultaService.consulta.examenClinico.pulso, [Validators.required, Validators.pattern("^[0-9]+$")]],
      temperatura: [this.consultaService.consulta.examenClinico.temperatura, [Validators.required, Validators.pattern("^[0-9]+$")]],
      frecuenciaCardiaca: [this.consultaService.consulta.examenClinico.frecuenciaCardiaca, [Validators.required, Validators.pattern("^[0-9]+$")]],
      frecuenciaRespiratoria: [this.consultaService.consulta.examenClinico.frecuenciaRespiratoria, [Validators.required, Validators.pattern("^[0-9]+$")]],
      examenClinicoGeneral: [this.consultaService.consulta.examenClinico.examenClinicoGeneral, [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
      examenClinicoOdontologico: [this.consultaService.consulta.examenClinico.examenClinicoOdontologico, [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
    })
  }


  bindDataIfExist()
  {
    this.consultaService.getById(this.idconsulta).subscribe((consulta:Consulta)=>{


    })
  }


  regresar() {

    this.consultaService.setExamenClinico(this.idconsulta, this.form.value).then(data => {

      this.location.back();

    }).catch(e => {

    })

  }

  setExamenClinico() {
    if (this.id !== null) {
      this.consultaService.getExamenClinicio(this.id).subscribe(data => {


        if(data.payload.data()?.examenClinico['pulso'])
        {
          this.form.setValue({
            pulso: data.payload.data().examenClinico['pulso'],
            temperatura: data.payload.data().examenClinico['temperatura'],
            frecuenciaCardiaca: data.payload.data().examenClinico['frecuenciaCardiaca'],
            frecuenciaRespiratoria: data.payload.data().examenClinico['frecuenciaRespiratoria'],
            examenClinicoGeneral: data.payload.data().examenClinico['examenClinicoGeneral'],
            examenClinicoOdontologico: data.payload.data().examenClinico['examenClinicoOdontologico']
          })
        }

      })
    }
  }


}

