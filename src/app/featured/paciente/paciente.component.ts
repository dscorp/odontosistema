import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  form1: FormGroup;
  form2: FormGroup;
  isEditable = false;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.form1 = this._formBuilder.group({
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      sexo: ['', Validators.required],
      edad: ['', Validators.required],
      lugar_nacimiento: ['', Validators.required],
      fecha_nacimiento: ['', Validators.required],
    });

    this.form2 = this._formBuilder.group({
      direccion: ['', Validators.required],
      procedencia: ['', Validators.required],
      ocupacion: ['', Validators.required],
      viajes: ['', Validators.required],
      telefono: ['', Validators.required],
      telefono_referencia: ['', Validators.required],
      referencia: ['', Validators.required],
    });
    
  }

}
