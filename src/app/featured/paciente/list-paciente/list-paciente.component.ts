import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatTableDataSource } from '@angular/material/table';
import { Buscar } from 'src/app/shared/model/buscar';
import { ConsultaService } from 'src/app/shared/services/consulta.service';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { Utils } from 'src/app/shared/utils/utlis';

@Component({
  selector: 'app-list-paciente',
  templateUrl: './list-paciente.component.html',
  styleUrls: ['./list-paciente.component.css'],
})
export class ListPacienteComponent implements OnInit {
  public buscar = new Buscar();
  consultaPacienteMotivos: any[] = [];
  displayedColumns = ['nombre','dni','fecha', 'telefono'];
  datasource: MatTableDataSource<any>;

  dataAsArray = new Array<any>();

  flagPaginado = true;
  flagLoading = false;
  servicios = [];
  ultimoDocumento: any = null;

    fechaInicio:Date;
    fechaFin:Date;
    servicio:string='';
  items: any[];

  constructor(
    private firestore: AngularFirestore,
    private listarPacientePorMotivo: ConsultaService,
    private datepipe:DatePipe
  ) {
    this.servicios = Utils.servicios;
  }

  // paginarsiguientes(ultimoDocumento, registrosAMostrar){
  //   const query = this.ref.ref.orderBy('dni', 'asc').startAfter(ultimoDocumento);
  //   return query.limit(registrosAMostrar).get();
  // }

  ngOnInit(): void {
    // this.listarConsultas(10);
  }


  generarReporte() {
    if (this.items.length > 0) {
      var doc = new jsPDF();

      const arr: any[] = [];

      this.items.forEach((tratamiento: any) => {
        const datepipe = new Date();
        let latest_date = this.datepipe.transform(tratamiento.fecha).toString();


        arr.push([tratamiento.nombreCompleto, tratamiento.dniPaciente,tratamiento.telefonoPaciente,latest_date]);
      });

      doc.text('HISTORIAL POR SERVICIO '+this.servicio, 15, 15);
      doc.text(
        'Del: ' +
        this.datepipe.transform(this.fechaInicio).toString()+
          ' al ' +
          this.datepipe.transform(this.fechaFin).toString() ,
        15,
        25
      );
      doc.autoTable({
        head: [['Nombre', 'Dni','Telefono','Fecha']],
        body: arr,
        margin: { top: 30 },
      });
      doc.save('historial.pdf');
    } else {
      Swal.fire(
        'Debe buscar datos para poder generar el reporte',
        '',
        'warning'
      );
    }
  }

  covertirdatadirestoreArray(data: any) {
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  covertirdatosparabusqueda(data: any) {
    this.dataAsArray = new Array<any>();
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  filtrar(form: NgForm) {
    if (form.valid) {
      const value = form.value;
      this.listarPacientePorMotivo.searchMotivo(this.servicio,this.fechaInicio.getTime(),this.fechaFin.getTime()).subscribe((data: any) => {
       this.items = this.covertirdatosparabusqueda(data);

        this.datasource = new MatTableDataSource(this.items)
        console.log(this.datasource);
      })
    }



  }

  listarConsultas(registrosAMostrar) {
    if (this.flagPaginado) {
      this.flagLoading = true;
      this.listarPacientePorMotivo
        .paginarsiguientes(this.ultimoDocumento, registrosAMostrar)
        .subscribe((data: any) => {
          console.log('paginando');
          this.ultimoDocumento = data.docs[data.docs.length - 1];

          let items = this.covertirdatadirestoreArray(data);
          this.datasource = new MatTableDataSource(items);
          this.flagLoading = false;
          console.log(this.datasource.data);
          if (data.docs.length < registrosAMostrar) {
            this.flagPaginado = false;
            console.log('ya no hay mas datos');
            this.flagLoading = false;
          }
        });
    }
  }
  scrollHandler(e) {
    switch (e) {
      case 'top':
        break;

      case 'bottom':
        this.listarConsultas(10);

        break;
    }
  }
  applyFilter(valor: string) {
    this.datasource.filter = valor.trim().toLowerCase();
  }




  downloadPDF() {
    // Extraemos el
    const DATA = document.getElementById('htmlData');
    const doc = new jsPDF('p', 'pt', 'a4');
    const options = {
      background: 'white',
      scale: 3,
    };
    html2canvas(DATA, options)
      .then((canvas) => {
        const img = canvas.toDataURL('image/PNG');

        // Add image Canvas to PDF
        const bufferX = 15;
        const bufferY = 15;
        const imgProps = (doc as any).getImageProperties(img);
        const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        doc.addImage(
          img,
          'PNG',
          bufferX,
          bufferY,
          pdfWidth,
          pdfHeight,
          undefined,
          'FAST'
        );
        return doc;
      })
      .then((docResult) => {
        docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
      });
  }
}
