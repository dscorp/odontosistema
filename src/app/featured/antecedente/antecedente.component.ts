import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Antecedente } from 'src/app/shared/model/antecedente';

@Component({
  selector: 'app-antecedente',
  templateUrl: './antecedente.component.html',
  styleUrls: ['./antecedente.component.css']
})
export class AntecedenteComponent implements OnInit {

  antecedente = new Antecedente();

  @Input()
  lstantecedentes: Array<Antecedente>;

  constructor() { }

  ngOnInit(): void {
  }

  push(form: NgForm) {
    if (form.valid) {
      console.log(this.antecedente);
      this.lstantecedentes.push(this.antecedente);
      this.antecedente = new Antecedente();
      form.resetForm();
    }
  }

  eliminar(index) {
    this.lstantecedentes.splice(index, 1);
  }

}
