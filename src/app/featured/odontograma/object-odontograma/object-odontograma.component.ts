import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Diente } from 'src/app/shared/model/diente';

@Component({
  selector: 'app-object-odontograma',
  templateUrl: './object-odontograma.component.html',
  styleUrls: ['./object-odontograma.component.css']
})
export class ObjectOdontogramaComponent implements OnInit {

  @Input()
  diente: Diente;

  constructor() {
  }

  ngOnInit(): void {
  }

}
