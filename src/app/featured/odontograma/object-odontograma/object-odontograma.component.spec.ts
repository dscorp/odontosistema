import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectOdontogramaComponent } from './object-odontograma.component';

describe('ObjectOdontogramaComponent', () => {
  let component: ObjectOdontogramaComponent;
  let fixture: ComponentFixture<ObjectOdontogramaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectOdontogramaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectOdontogramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
