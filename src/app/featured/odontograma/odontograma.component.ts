import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { Diente } from 'src/app/shared/model/diente';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import { ConfdienteComponent } from './confdiente/confdiente.component';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-odontograma',
  templateUrl: './odontograma.component.html',
  styleUrls: ['./odontograma.component.css']
})

export class OdontogramaComponent implements OnInit {

  dientes_cua1 : Diente[] = [
    {numero : 18},
    {numero : 17},
    {numero : 16},
    {numero : 15},
    {numero : 14},
    {numero : 13},
    {numero : 12},
    {numero : 11},
    {numero : 21},
    {numero : 22},
    {numero : 23},
    {numero : 24},
    {numero : 25},
    {numero : 26},
    {numero : 27},
    {numero : 28},
  ]

  dientes_cua2 : Diente[] = [
    {numero : 55},
    {numero : 54},
    {numero : 53},
    {numero : 52},
    {numero : 51},
    {numero : 61},
    {numero : 62},
    {numero : 63},
    {numero : 64},
    {numero : 65},
  ]

  dientes_cua3 : Diente[] = [
    {numero : 85},
    {numero : 84},
    {numero : 83},
    {numero : 82},
    {numero : 81},
    {numero : 71},
    {numero : 72},
    {numero : 73},
    {numero : 74},
    {numero : 75},
  ]

  dientes_cua4 : Diente[] = [
    {numero : 48},
    {numero : 47},
    {numero : 46},
    {numero : 45},
    {numero : 44},
    {numero : 43},
    {numero : 42},
    {numero : 41},
    {numero : 31},
    {numero : 32},
    {numero : 33},
    {numero : 34},
    {numero : 35},
    {numero : 36},
    {numero : 37},
    {numero : 38},
  ]
  idconsulta: string;
  nombrePaciente : string;

  constructor(
    public consultaService: ConsultaService,
    public matDialogo: MatDialog,
    private location: Location,
    private route: ActivatedRoute,
    private router:Router,
    ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.idconsulta = params.get('id');
    });
    this.load();
  }

  load(){
    this.consultaService.getById(this.idconsulta).subscribe(data =>{
      this.nombrePaciente = data["nombres"]+" "+" "+data["apellidos"];
      //Save in list odontogram
      this.consultaService.consulta.listDeOdontogramas = data["listDeOdontogramas"]
      data["listDeOdontogramas"].forEach( function(d : Diente){
        this.loadDientes(d);
      }.bind(this))
    })
   }

  loadDiente(cua_x:Diente[] ,d: Diente){
    cua_x.forEach(function(x, index) {
      if (x.numero === d.numero) {
        cua_x[index] = d;
      }
    });
  }

  loadDientes(d:Diente){

    var numero = d.numero;

    if(numero >= 11 && numero <= 28){
      this.loadDiente(this.dientes_cua1,d);
    }

    if(numero >= 51 && numero <= 65){
      this.loadDiente(this.dientes_cua2,d);
    }

    if(numero >= 71 && numero <= 85){
      this.loadDiente(this.dientes_cua3,d);
    }

    if(numero >= 31 && numero <= 48){
      this.loadDiente(this.dientes_cua4,d);
    }
  }

  select(diente:Diente): void{

    const dialogoconfig = new MatDialogConfig();
    dialogoconfig.id = "modal-componente";
    dialogoconfig.height = "500px";
    dialogoconfig.width = "700px";
    dialogoconfig.data = diente;

    const modalDialog = this.matDialogo.open(ConfdienteComponent, dialogoconfig);

    modalDialog.afterClosed().subscribe(result => {
      if(result == null){
        return;
      }
      if(result.event == 'Ok'){

        //Verificar si el diente existe en la lista de odontograma
       var pdiente = this.consultaService.consulta.listDeOdontogramas.findIndex((sdiente:Diente) => sdiente.numero === diente.numero);  
      
        
        if(pdiente == -1){
          //Si no existe añadir
          this.consultaService.consulta.listDeOdontogramas.push(result.data);
        }else{
          //Si existe reemplazar
          this.consultaService.consulta.listDeOdontogramas[pdiente] = result.data;
        }

      }
    });
  }

  filterDientes(d : Diente[], cua_x:Diente[], attrib: boolean) {
    cua_x.forEach(function(diente){
      if(attrib == true){
        //Especificacion
        if(diente.especificacion != null){
          d.push(diente);
        }
      }else{
        //Observacion
        if(diente.observacion != null){
          d.push(diente);
        }
      }
    });
  }

  dientesEspecificacion(){
      let dientes : Diente[] = new Array();
      this.filterDientes( dientes, this.dientes_cua1, true);
      this.filterDientes( dientes, this.dientes_cua2, true);
      this.filterDientes( dientes, this.dientes_cua3, true);
      this.filterDientes( dientes, this.dientes_cua4, true);
      return dientes;
  }

  dientesObservacion(){
    let dientes : Diente[] = new Array();
    this.filterDientes( dientes, this.dientes_cua1, false);
    this.filterDientes( dientes, this.dientes_cua2, false);
    this.filterDientes( dientes, this.dientes_cua3, false);
    this.filterDientes( dientes, this.dientes_cua4, false);
    return dientes;
  }

  downloadPDF(): void {
        // Extraemos el
        const DATA = document.getElementById('htmlData');
        const doc = new jsPDF('l', 'pt', 'a4');
        
        doc.text(40, 20, "Odontograma - " + this.nombrePaciente);

        doc.text("Especificacion :",40,350);
        var dienteE = this.dientesEspecificacion();
        dienteE.forEach(function (d : Diente,index){
          doc.text("#"+ d.numero + " " + d.especificacion,40,350+ ((index+1)*20)) 
        });

        doc.text("Observacion :",390,350);
        var dienteO = this.dientesObservacion();
        dienteO.forEach(function (d : Diente,index){
          doc.text("#"+ d.numero + " " + d.observacion,390,350+ ((index+1)*20)) 
        });

        const options = {
          background: 'white',
          scale: 3
        };
        html2canvas(DATA, options).then((canvas) => {
          const img = canvas.toDataURL('image/PNG');
          // Add image Canvas to PDF
          const bufferX = 45;
          const bufferY = 45;
          const imgProps = (doc as any).getImageProperties(img);
          const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
          const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
          doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
          return doc;
        }).then((docResult) => {
          docResult.save(`${new Date().toISOString()}.pdf`);
        });

  }

  report(): void{
    this.downloadPDF();
  }

  finish() : void {
    this.consultaService.setodontograma(this.idconsulta, this.consultaService.consulta.listDeOdontogramas).then(data => {
      Swal.fire({
        title:"Odontograma se guardo exitosamente",
        icon: 'success',
      }).then(function() {
        this.router.navigate(['dental/contenedorconsulta/'+this.idconsulta+"/"+this.nombrePaciente])
      }.bind(this));
    }).catch(e => {
      console.log('error', e);
    })
  }

}
