import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Diente } from 'src/app/shared/model/diente';

@Component({
  selector: 'app-confdiente',
  templateUrl: './confdiente.component.html',
  styleUrls: ['./confdiente.component.css']
})
export class ConfdienteComponent implements OnInit {

  colors:string = "#ffffff";

  constructor(public dialogRef: MatDialogRef<ConfdienteComponent>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: Diente) {}

  ngOnInit(): void {
  }

  actionFunction(){
    this.dialogRef.close({event:"Ok", data:this.data})
  }

  closeModal(){
    this.dialogRef.close({event:'Cancel'});
  }

  select(id:number){

    if(id == 1){
      this.data.cuadrante_1 = this.colors;
    }
    if(id == 2){
      this.data.cuadrante_2 = this.colors;
    }
    if(id == 3){
      this.data.cuadrante_3 = this.colors;
    }
    if(id == 4){
      this.data.cuadrante_4 = this.colors;
    }
    if(id == 5){
      this.data.cuadrante_5 = this.colors;
    }

    console.log("Se ha selecionado el cuadrante ");
    console.log(id);
    console.log(this.data);
  }
}
