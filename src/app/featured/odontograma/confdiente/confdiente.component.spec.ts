import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfdienteComponent } from './confdiente.component';

describe('ConfdienteComponent', () => {
  let component: ConfdienteComponent;
  let fixture: ComponentFixture<ConfdienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfdienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfdienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
