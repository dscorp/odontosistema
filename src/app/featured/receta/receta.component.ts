import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Receta } from 'src/app/shared/model/receta';
import Swal from 'sweetalert2';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-receta',
  templateUrl: './receta.component.html',
  styleUrls: ['./receta.component.css']
})
export class RecetaComponent implements OnInit {

  receta = new Receta();
  lstrecetas = new Array<Receta>();


  @Output()
  propagar = new EventEmitter<Array<Receta>>();
  idconsulta: string;

  constructor(
    public consultaService: ConsultaService,
    private _formBuilder: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.idconsulta = params.get('id');
      this.cargarEnfermedadesActuales()
    });
  }
  cargarEnfermedadesActuales()
  {


    if(this.consultaService.consultaDesdeFirebase?.listaDeRecetas)
    {
      this.consultaService.consulta.listaDeRecetas=this.consultaService.consultaDesdeFirebase.listaDeRecetas;
    }
  }
  emitirResultado() {
    if (this.lstrecetas.length == 0) {
      Swal.fire({
        title: "Debe agregar almenos una receta",
        icon: "error"
      })
    }
    else {
      //emitir resutlado
     this.propagar.emit(this.lstrecetas);
    }
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.consultaService.consulta.listaDeRecetas.push(form.value)
      this.receta = new Receta();
      form.resetForm();
    }
  }


  elimiarDeArray(index) {
    this.consultaService.consulta.listaDeRecetas.splice(index, 1)
  }


  regresar()
  {

    this.consultaService.setReceta(this.idconsulta, this.consultaService.consulta.listaDeRecetas).then(data => {
      console.log('actualizacion exitosa', data);

      this.location.back();

    }).catch(e => {
      console.log('error', e);
    })

  }
}
