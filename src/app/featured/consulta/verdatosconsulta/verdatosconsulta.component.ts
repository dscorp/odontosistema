import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Consulta } from 'src/app/shared/model/consulta';
import { Paciente } from 'src/app/shared/model/paciente';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import { PacienteService } from 'src/app/shared/services/paciente.service';

@Component({
  selector: 'app-verdatosconsulta',
  templateUrl: './verdatosconsulta.component.html',
  styleUrls: ['./verdatosconsulta.component.css']
})
export class VerdatosconsultaComponent implements OnInit {

  consulta : Consulta = new Consulta();
  paciente : Paciente = new Paciente();
  exaArrayClinica : any[] = []

  constructor(
    private activateRoute: ActivatedRoute,
    private consultaService: ConsultaService,
    private pacienteService: PacienteService,
  ) { }

  ngOnInit(): void {
    const id  = this.activateRoute.snapshot.params.id;
    this.getConsulta(id);
    this.exaArray(id);
  }

  getPaciente(id:string){
    this.pacienteService.getById(id).subscribe((data:any) => {
      this.paciente = data;
    })
  }

  getConsulta(id:string){
    this.consultaService.getById(id).subscribe((data:any) => {
      this.consulta = data;
      console.log(this.consulta);
      this.getPaciente(this.consulta.id_paciente);
    })
  }

  exaArray(id:string){
    this.consultaService.getById(id).subscribe((data : any) => {

      this.exaArrayClinica = data.examenClinico;
      this.exaArrayClinica = Array.of(this.exaArrayClinica)
      console.log('por sia aca')
      console.log(this.exaArrayClinica)
    })
  }
}
