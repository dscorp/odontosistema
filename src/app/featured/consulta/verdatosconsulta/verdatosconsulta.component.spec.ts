import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerdatosconsultaComponent } from './verdatosconsulta.component';

describe('VerdatosconsultaComponent', () => {
  let component: VerdatosconsultaComponent;
  let fixture: ComponentFixture<VerdatosconsultaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerdatosconsultaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerdatosconsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
