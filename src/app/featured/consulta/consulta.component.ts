import { AngularFirestore } from '@angular/fire/firestore';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Form, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { MatTableDataSource } from '@angular/material/table';
import { Consulta } from 'src/app/shared/model/consulta';
import {Antecedente} from 'src/app/shared/model/antecedente';
import { CitasService } from 'src/app/shared/services/citas.service';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import Swal from 'sweetalert2';
import { PacienteService } from 'src/app/shared/services/paciente.service';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/shared/model/paciente';
// import * as admin from 'firebase-admin';
@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  @ViewChild('stepper') private stepper: MatStepper;
  @ViewChild('nombres') private inputNombres : ElementRef;

  dni_search_fired:Boolean = false;
  antecedente : FormGroup;
  dataSource: MatTableDataSource<any>;
  antecedentes: Array<Antecedente> = [];

  old_id_paciente: string = "";

  //Variables para manejar el desabilitado de inputs
  disable_data1 : Boolean = false;
  disable_data2 : Boolean = false;

  consulta = new Consulta();
  paciente = new Paciente();
  listadeAntecedentes = new Array<Antecedente>();

  constructor(
    public consultaService : ConsultaService,
    private citaService: CitasService,
    private pacienteService: PacienteService,
    private _formBuilder: FormBuilder,
    private router:Router,
    private firestore:AngularFirestore,
  ) {
      this.paciente.sexo = "M";
    }

  ngOnInit(): void {
    this.antecedente = this._formBuilder.group({
      referencia : ['', Validators.required],
      tipo : ['' , Validators.required]
    });
  }

  save() {
    console.log("Consulta a guardar");
    var paciente_nuevo : Boolean = this.old_id_paciente == "";
    if(paciente_nuevo){
      const id_paciente  = this.firestore.createId();
      this.consulta.id_paciente = id_paciente;  
    }else{
      this.consulta.id_paciente = this.old_id_paciente;
    }

    const id_consulta  = this.firestore.createId();
    this.consultaService.save(this.consulta,this.paciente,id_consulta,paciente_nuevo).then(data => {
      Swal.fire({
        title:"Consulta registrada exitosamente",
        icon: 'success',
      }).then(function() {
        this.router.navigate(['dental/contenedorconsulta/'+id_consulta+"/"+this.paciente.nombres+" "+this.paciente.apellidos])
      }.bind(this));
    }).catch(e => {
      Swal.fire({
        title:"Ocurrio un error al registrar el pago",
        icon: 'error',
        text: e.message,
      })
    })
  }

  back(){
    this.stepper.previous();
  }

  next(){
    console.log("btn next");
    //Finalizar
    if(this.stepper.selectedIndex == 3){
      //Save
      this.save();
    }else{
      this.stepper.next();
    }

  }

  keyup(){
    this.dni_search_fired = false;
  }

  focusout(dni:string ,v: any ){

    //Verificar errores
    if(v != null){
      console.log("Existen errores");
      return;
    }

    //Si la longitud es 8 buscar el DNI
    if(dni.length == 8){
      console.log("Longitud 8")
      this.search(dni,v);
      this.dni_search_fired = false;
    }
  }

  search(dni:string, v: any){

    console.log("this");
    console.log(v);

    console.log("DNI " + dni);

    //Verificar que solo se presione una vez
    if(this.dni_search_fired == false){
      this.dni_search_fired = true;
    }else{
      return;
    }

    //Verificar errores
    if(v != null){
      console.log("Existen errores");
      return;
    }

    var promesa1 = this.pacienteService.search(dni).toPromise().then(data=> {
      console.log("Promesa 01");
      return data;
    });

    var promesa3= this.citaService.search(dni).toPromise().then(data=> {
      console.log("Promesa 03");
      return data;
    });

    var promesa2 = promesa1.then(data =>{
      console.log("promesa 02");
      if(data.docs.length > 0){
        console.log("Se encontro en paciente");
        this.setdata(data.docs[0].data());
        this.setdata2(data.docs[0].data());
        this.old_id_paciente = data.docs[0].id;
      }else{
        console.log("Buscar en promesa 3");
        promesa3.then(data => {
            if(data.docs.length > 0){
              console.log("Se encontro en cita");
              // se encontro en paciente
              this.setdata(data.docs[0].data());

              console.log("Se cambio index");
              this.stepper.selectedIndex = 2;
              console.log("Index es");
              console.log(this.stepper.selectedIndex);

            }else{
              console.log("No se encontro ni en cita ni en paciente");

              //Clean data
              this.cleandata();
              this.disable_data1 = false;
              this.disable_data2 = false;
              this.old_id_paciente = "";

              //Setear focus en el campo "nombres"
              this.inputNombres.nativeElement.focus();
            }
        });
      }
    });

    promesa2.then();
  }

  setdata(data){
    this.stepper.linear = false;
    this.paciente.nombres = data.nombres;
    this.paciente.apellidos = data.apellidos;
    this.paciente.sexo = data.sexo ;
    this.paciente.edad = data.edad ;
    this.paciente.telefono = data.telefono ;
    this.stepper.selectedIndex = 1;
    this.stepper.linear= true;

    this.disable_data1 = true;
  }

  setdata2(data){
    this.stepper.linear = false;
    this.paciente.direccion = data.direccion ;
    this.paciente.procedencia = data.procedencia ;
    this.paciente.ocupacion = data.ocupacion ;
    this.paciente.viajes = data.viajes ;
    this.paciente.telefono_referencia = data.telefono_referencia ;
    this.paciente.referencia = data.referencia ;
    this.paciente.lugar_nacimiento = data.lugar_nacimiento ;
    this.paciente.fecha_nacimiento = data.fecha_nacimiento ;
    this.stepper.selectedIndex = 2;
    this.stepper.linear= true;

    this.disable_data2 = true;
  }

  cleandata(): void{
    this.paciente.nombres = "";
    this.paciente.apellidos = "";
    this.paciente.sexo = "M" ;
    this.paciente.edad = null ;
    this.paciente.telefono = "" ;
    this.paciente.direccion = "" ;
    this.paciente.procedencia = "" ;
    this.paciente.ocupacion = "" ;
    this.paciente.viajes = "" ;
    this.paciente.telefono_referencia = "" ;
    this.paciente.referencia = "" ;
    this.paciente.lugar_nacimiento = "" ;
    this.paciente.fecha_nacimiento = "" ;
  }

}

