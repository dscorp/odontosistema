import { ActivatedRoute } from '@angular/router';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import { NgForm } from '@angular/forms';
import { EnfermedadActual } from './../../../shared/model/enfermedadactualmodel';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-enfermedadesactuales',
  templateUrl: './enfermedadesactuales.component.html',
  styleUrls: ['./enfermedadesactuales.component.css']
})
export class EnfermedadesactualesComponent implements OnInit {


  enfermedad = new EnfermedadActual();



  @Output()
  propagar = new EventEmitter<Array<EnfermedadActual>>();
  idconsulta: string;


  constructor(
    public consultaService:ConsultaService,
    private location: Location,
    private route: ActivatedRoute,
    )
    {

  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.idconsulta = params.get('id');
     this.cargarEnfermedadesActuales()
    });

  }


cargarEnfermedadesActuales()
{

  console.log(this.consultaService.consultaDesdeFirebase?.listaDeEnfermedades);
  if(this.consultaService.consultaDesdeFirebase?.listaDeEnfermedades)
  {
    this.consultaService.consulta.listaDeEnfermedades=this.consultaService.consultaDesdeFirebase.listaDeEnfermedades;
  }
}

  onSubmit(form: NgForm) {


    if (form.valid) {
      this.consultaService.consulta.listaDeEnfermedades.push(form.value)
      this.enfermedad = new EnfermedadActual();
      form.resetForm();
    }
  }


  elimiarDeArray(index) {
    this.consultaService.consulta.listaDeEnfermedades.splice(index, 1)
  }


  regresar()
  {

    this.consultaService.setEnfermedaesActuales(this.idconsulta, this.consultaService.consulta.listaDeEnfermedades).then(data => {
      console.log('actualizacion exitosa', data);

      this.location.back();

    }).catch(e => {
      console.log('error', e);
    })

  }

  // emitirResultado() {
  //   if (this.consultaService.consulta.listaDeEnfermedades.length == 0) {
  //     Swal.fire({
  //       title: "Debe agregar almenos una enfermedad",
  //       icon: "error"
  //     })
  //   }
  //   else {
  //     //emitir resutlado
  //    this.propagar.emit(this.listaDeEnfermedades);
  //   }
  // }

}
