import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Buscar } from 'src/app/shared/model/buscar';
import { ConsultaService } from 'src/app/shared/services/consulta.service';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import html2canvas from 'html2canvas';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { DataSource } from '@angular/cdk/table';


@Component({
  selector: 'app-historial-reporte',
  templateUrl: './historial-reporte.component.html',
  styleUrls: ['./historial-reporte.component.css'],
})
export class HistorialReporteComponent implements OnInit {
  buscar = new Buscar();

  historial: any[] = [];

  recorrido;

  displayedColumns = ['dni', 'nombre', 'fecha', 'motivo'];
  dataSource: MatTableDataSource<any>;

  dataAsArray = new Array<any>();

  constructor(
    private consultaService: ConsultaService,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {}
  covertirdatadirestoreArray(data: any) {
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  covertirdatosparabusqueda(data: any) {
    this.dataAsArray = new Array<any>();
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  searchdni(dni: string) {
    if (dni.length == 8) {
      this.consultaService.search(dni).subscribe((data: any) => {
        this.historial = this.covertirdatosparabusqueda(data);
        this.dataSource = new MatTableDataSource(this.historial);
      });
    } else if (dni.length == 0) {
      this.historial = [];
      this.dataSource = new MatTableDataSource(this.historial);
      // return this.dataSource.sort
    }
  }

  // listarHistorial(){
  //   this.consultaService.read().subscribe(data => {
  //     this.historial = this.covertirdatadirestoreArray(data);
  //     this.dataSource = new MatTableDataSource(this.historial);
  //   })
  // }

  applyFilter(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  generarReporte() {
    if (this.historial.length > 0) {
      var doc = new jsPDF();

      const arr: any[] = [];

      this.historial.forEach((historial: any) => {
        const datepipe = new Date();
        let latest_date = this.datepipe.transform(historial.fecha).toString();
        arr.push([latest_date, historial.motivo]);
      });

      doc.text('HISTORIAL DE CONSULTAS', 15, 15);
      doc.text(
        'PACIENTE: ' +
          this.historial[0].nombres +
          ' ' +
          this.historial[0].apellidos +
          '      DNI:' +
          this.historial[0].dni,
        15,
        25
      );
      doc.autoTable({
        head: [['Fecha', 'Motivo']],
        body: arr,
        margin: { top: 30 },
      });
      doc.save('historial.pdf');
    } else {
      Swal.fire(
        'Debe buscar datos para poder generar el reporte',
        '',
        'warning'
      );
    }
  }

  generarReporteHistoriaClinica(){

    const doc = new  jsPDF()

    doc.text('HISTORIAL CLINICA: ' , 15, 15);
    doc.text('PACIENTE: ', 15, 25)

    doc.autoTable({ html: '#historial', margin: { top: 40 } })

    // doc.autoTable({
    //   margin: { top: 40 },
    //   head: [['dni','nombre','fecha', 'motivo']],
    //   body:   this.dataAsArray.values
    // });

    doc.save('historialClinca.pdf')
  }
}
