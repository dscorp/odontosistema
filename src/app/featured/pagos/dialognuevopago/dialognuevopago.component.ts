import { Pago } from 'src/app/shared/model/pago';
import { PagoService } from './../../../shared/services/pago.service';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dialognuevopago',
  templateUrl: './dialognuevopago.component.html',
  styleUrls: ['./dialognuevopago.component.css'],
})
export class DialognuevopagoComponent implements OnInit {
  conceptoform: string;
  monto: number;
  constructor(
    public dialogRef: MatDialogRef<DialognuevopagoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogNuevoPagoData,
    private PagoService: PagoService
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  limpiarCampos()
  {
    this.conceptoform="";
    this.monto=null;
  }
  submit(form: NgForm) {
    const dataform: any = form.value;
    const pago = new Pago();
    pago.concepto = dataform.conceptoForm;
    pago.monto = dataform.monto;
    pago.fecha=new Date().getTime();
    pago.idConsulta=this.data.idConsulta;
    pago.isPagoDeTratamiento=false;
    pago.dniPaciente=this.data.dniPaciente;
    this.PagoService.save(pago).then(it =>{
      Swal.fire("Pago registrado exitosamente")

      this.limpiarCampos()
      this.dialogRef.close();
    },err =>{
          Swal.fire("no se pudo registrar el pago")
          this.limpiarCampos()
          this.dialogRef.close();
    });
  }
  ngOnInit(): void {}
}
export interface DialogNuevoPagoData {
  idConsulta: string;
  dniPaciente:string;
}

