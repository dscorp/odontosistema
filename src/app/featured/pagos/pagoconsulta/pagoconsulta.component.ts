import { ConsultaService } from './../../../shared/services/consulta.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { PagoService } from '../../../shared/services/pago.service';
import { Component, OnInit } from '@angular/core';
import { Pago } from 'src/app/shared/model/pago';
import Swal from 'sweetalert2';
import { DialognuevopagoComponent } from '../dialognuevopago/dialognuevopago.component';
import { Consulta } from 'src/app/shared/model/consulta';
@Component({
  selector: 'app-pagocita',
  templateUrl: './pagoconsulta.component.html',
  styleUrls: ['./pagoconsulta.component.css'],
})
export class PagoConsultaComponent implements OnInit {
  dataAsArray = new Array<any>();
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['Concepto', 'Monto', 'Fecha'];
  loadingFlag: boolean = false;
  public pago = {} as Pago;
  public regexeEnteros = '/^d+$/';
  idconsulta: string;
  dniPaciente: string;
  selectedConsulta: Consulta;

  constructor(
    private pagoService: PagoService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private consultaService: ConsultaService
  ) {
    this.selectedConsulta = this.consultaService.selectedConsulta;
    if (this.selectedConsulta == null) {
      router.navigate(['dental/consultas']);
      return;
    }
    this.idconsulta = this.selectedConsulta.id;
    this.dniPaciente = this.selectedConsulta.dni;

    this.pago.fecha = new Date().getTime();
    this.pago.idPlanTratamiento = '';
  }

  ngOnInit(): void {
    this.getByConsulta(this.selectedConsulta.id);
    this.loadingFlag = true;
  }

  covertirdatadirestoreArray(data: any) {
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialognuevopagoComponent, {
      width: '357px',
      data: { idConsulta: this.idconsulta, dniPaciente: this.dniPaciente },
    });
    dialogRef.afterClosed().subscribe((result) => { });
  }

  getByConsulta(idConsulta: string) {
    return this.pagoService
      .getByidConsulta(idConsulta)
      .valueChanges()
      .subscribe(
        (items) => {
          console.log('resultados');
          console.table(items);
          this.dataSource = new MatTableDataSource(items);
          this.loadingFlag = false;
        },
        (err) => {
          this.loadingFlag = false;
        }
      );
  }

  save(pago) {
    this.pagoService
      .save(this.pago)
      .then((data) => {
        Swal.fire({
          title: 'Pago registrado exitosamente',
          icon: 'success',
        });
        this.pago = {} as Pago;
        // this.pago.tipoPago = "consulta"
        this.pago.fecha = new Date().getTime();
        this.pago.idPlanTratamiento = '';
      })
      .catch((e) => {
        Swal.fire({
          title: 'Ocurrio un error al registrar el pago',
          icon: 'error',
          text: e.message,
        });
      });
  }

  onSubmit(form) {
    if (form.valid) {
      this.save(this.pago);
    }

    console.log(form);
  }
}
