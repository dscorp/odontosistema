import { Pago } from './../../../shared/model/pago';
import { PagoService } from './../../../shared/services/pago.service';
import { Paciente } from './../../../shared/model/paciente';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Utils } from 'src/app/shared/utils/utlis';
import { PacienteService } from './../../../shared/services/paciente.service';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-listar-pagos-paciente',
  templateUrl: './listar-pagos-paciente.component.html',
  styleUrls: ['./listar-pagos-paciente.component.css'],
})
export class ListarPagosPacienteComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = [
    'nombres',
    'dni',
    'edad',
    'direccion',
    'botones',
  ];
  dataSource: MatTableDataSource<Paciente>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private pacienteService: PacienteService,
    private pagoService: PagoService,
    public datepipe: DatePipe
  ) {
    // this.listarpacientes()
  }

  generarReporte(paciente) {

    var doc = new jsPDF();

    this.pagoService.getByDniPaciente(paciente.dni).valueChanges().subscribe(
      (pagos:any)=> {

        const arr:any[] = [];

        pagos.forEach((pagoo:Pago) => {
          const datepipe = new Date();
          let latest_date =this.datepipe.transform(pagoo.fecha).toString();
          arr.push([pagoo.monto.toString(),pagoo.concepto,latest_date ])
        });

        doc.text('RECORD DE PAGOS', 15, 15);
        doc.text('PACIENTE: '+paciente.nombres+' '+paciente.apellidos+'      DNI:'+paciente.dni, 15, 25);
        doc.autoTable({
          head: [['Monto S/','Concepto', 'Fecha']],
          body: arr,
          margin: { top: 30 }

        });
        doc.save('table.pdf');
      }
    );
  }

  ngOnInit(): void {}

  listarpacientes() {
    this.pacienteService.get().subscribe((data) => {
      const pacientes = Utils.covertirdatafirestoreToArray(data);
      this.dataSource = new MatTableDataSource(pacientes);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ngAfterViewInit() {
    this.listarpacientes();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

/** Builds and returns a new User. */
// function createNewUser(id: number): any {
//   const name =
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))] +
//     ' ' +
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) +
//     '.';

//   return {
//     id: id.toString(),
//     name: name,
//     progress: Math.round(Math.random() * 100).toString(),
//     color: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
//   };
// }
