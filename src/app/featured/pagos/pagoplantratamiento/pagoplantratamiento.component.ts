import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { TratamientoService } from 'src/app/shared/services/tratamiento.service';
import Swal from 'sweetalert2';
import { Pago } from './../../../shared/model/pago';
import { PagoService } from './../../../shared/services/pago.service';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
@Component({
  selector: 'app-pagoplantratamiento',
  templateUrl: './pagoplantratamiento.component.html',
  styleUrls: ['./pagoplantratamiento.component.css'],
})
export class PagoplantratamientoComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['Concepto', 'Monto', 'Fecha'];

  // fecha: number = new Date().getTime();
  tratamiento: any;
  montoRestante: number;
  fechaParseada;

  public pago = {} as Pago;

  montoControl = new FormControl('', [Validators.required]);
  loadingFlag: boolean;

  constructor(
    private pagoService: PagoService,
    private tratamientoServie: TratamientoService
  ) {
    this.tratamiento = tratamientoServie.tratamientoSelected;
    this.listarPagosByPlanTratamiento(this.tratamiento.id);

    this.montoRestante =
      Number.parseFloat(this.tratamiento.costoTotal) -
      Number.parseFloat(this.tratamiento.montoAbonado);
    this.fechaParseada = new Date(this.tratamiento.fecha).toLocaleDateString();
  }






  generarReportePagos()
  {
    const doc = new jsPDF()

    doc.text('PAGOS DE TRATAMIENTO: '+this.tratamiento.nombreTratamiento.toUpperCase(), 15, 15);
    doc.text('PACIENTE: '+this.tratamiento.nombreCompleto, 15, 25);

    doc.text(' Presupuesto del tratamiento: '+this.tratamiento.costoTotal +'      Monto Abonado: '+this.tratamiento.montoAbonado, 15, 35);


    doc.autoTable({ html: '#pagostratamiento', margin: { top: 40  }   })

    doc.save('reportePagosTratamiento.pdf')
  }

  realizarPagoTratamiento() {
    if (this.montoControl.valid) {
      var pago = new Pago();

      pago.monto = this.montoControl.value;
      pago.fecha = new Date().getTime();
      pago.concepto = 'cuota plan de tratamiento';
      pago.idPlanTratamiento = this.tratamiento.id;
      pago.idConsulta = this.tratamiento.id_consulta;
      pago.isPagoDeTratamiento = true;
      pago.dniPaciente = this.tratamiento.dniPaciente;

      this.pagoService
        .save(pago)
        .then((response) => {
          Swal.fire('Pago registrado correctamente', '', 'success');

          //TODO atualizar datos de tratamiento al registrar pago
          this.tratamiento.montoAbonado =
            this.tratamiento.montoAbonado + pago.monto;
          this.tratamientoServie.actualizarMontosGeneralesTratamientoPagos(
            this.tratamiento
          );
        })
        .catch((err) => {
          Swal.fire('No se pudo registrar el pago', '', 'error');
        });
    }
  }

  listarPagosByPlanTratamiento(idPlanTratamiento: string) {
    this.loadingFlag = true;
    this.pagoService
      .getByidPlanTratamiento(idPlanTratamiento)
      .valueChanges()
      .subscribe(
        (items) => {
          this.dataSource = new MatTableDataSource(items);
          this.loadingFlag = false;
        },
        (err) => {
          this.loadingFlag = false;
        }
      );
  }
  ngOnInit(): void {}
}
