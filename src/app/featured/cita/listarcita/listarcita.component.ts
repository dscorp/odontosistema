import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Buscar } from 'src/app/shared/model/buscar';
import { CitasService } from 'src/app/shared/services/citas.service';
import { ConfirmaciondialogoComponent } from '../confirmaciondialogo/confirmaciondialogo.component';

@Component({
  selector: 'app-listarcita',
  templateUrl: './listarcita.component.html',
  styleUrls: ['./listarcita.component.css'],
})
export class ListarcitaComponent implements OnInit {
  public buscar = new Buscar();
  citas: any[] = [];
  displayedColumns = ['dni', 'nombre', 'telefono', 'fecha', 'hora', 'opciones'];
  dataSource: MatTableDataSource<any>;

  dataAsArray = new Array<any>();

  busqueda: string[] = ['dni', 'nombres'];
  flagPaginado = true;
  flagLoading = false;
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  ultimoDocumento: any = null;
  constructor(
    private citaService: CitasService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listarCitas(10);
  }

  covertirdatadirestoreArray(data: any) {
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }

  covertirdatosparabusqueda(data: any) {
    this.dataAsArray = new Array<any>();
    data.docs.map((doc) => {
      this.dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return this.dataAsArray;
  }
  searchdni(dni: string) {
    if (dni.length == 8) {

      this.citaService.search(dni).subscribe((data: any) => {
        let items = this.covertirdatosparabusqueda(data);
        this.dataSource = new MatTableDataSource(items);
      });

    } else if (dni.length == 0) {

      this.listarinicial()



    }
  }


listarinicial()
{
  this.ultimoDocumento = null;

  this.flagPaginado = true;
  this.flagLoading = false;

  this.citaService
  .paginarsiguientes(this.ultimoDocumento, 10)
  .then((data: any) => {
    this.dataAsArray = new Array<any>();
    this.dataSource = new MatTableDataSource(null);
    this.ultimoDocumento = data.docs[data.docs.length - 1];
    let items = this.covertirdatadirestoreArray(data);
    this.dataSource = new MatTableDataSource(items);
})
}

  listarCitas(registrosAMostrar) {

      if (this.flagPaginado) {
        this.flagLoading=true
        this.citaService
          .paginarsiguientes(this.ultimoDocumento, registrosAMostrar)
          .then((data: any) => {

            this.ultimoDocumento = data.docs[data.docs.length - 1];
            let items = this.covertirdatadirestoreArray(data);
            this.dataSource = new MatTableDataSource(items);
            this.flagLoading = false;
            if (data.docs.length < registrosAMostrar) {
              this.flagPaginado = false;
              this.flagLoading = false;
            }
          });
      }


  }

  delete(id: string) {
    const dialogRef = this.dialog.open(ConfirmaciondialogoComponent, {
      width: '350px',
      data: { mensaje: 'Esta seguro que desea eliminar la cita?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'aceptar') {
        this.citaService.delete(id);
        this.snackBar.open('La cita fue eliminado con exito!!', '', {
          duration: 1000,
        });
        this.dataSource=new MatTableDataSource()
        this.dataAsArray = new Array<any>();
        this.ultimoDocumento=null
        this.flagPaginado=true
        this.listarCitas(10);
      }
    });
  }

  applyFilter(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  scrollHandler(e) {
    switch (e) {
      case 'top':
        break;

      case 'bottom':
        this.listarCitas(10);

        break;
    }
  }

  editarCita(citas: any) {
    // console.log(citas);
    this.router.navigate(['dental/editarcita', citas.id]);
  }
}
