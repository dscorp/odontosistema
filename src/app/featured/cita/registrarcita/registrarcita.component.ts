import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Cita } from 'src/app/shared/model/cita';
import { CitasService } from 'src/app/shared/services/citas.service';
import { PacienteService } from 'src/app/shared/services/paciente.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrarcita',
  templateUrl: './registrarcita.component.html',
  styleUrls: ['./registrarcita.component.css']
})
export class RegistrarcitaComponent implements OnInit {

  cita = new Cita();

  form : FormGroup;

  id : string | null;

  titulo = 'Registrar nueva cita'

  dni_search_fired: Boolean = false;

  constructor(
    private fb : FormBuilder,
     private citaService: CitasService,
     private pacienteService: PacienteService,
     private aRoute: ActivatedRoute,
     private router: Router,
  ) {
    this.id = this.aRoute.snapshot.paramMap.get('id');
    console.log('hola a aca es');
    console.log(this.id);
   }

  ngOnInit(): void {
    this.form = this.fb.group({
      dni : ['', [Validators.required,Validators.pattern("^[0-9]+$"), Validators.minLength(8), Validators.maxLength(8)]],
    nombres : ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
    apellidos : ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
    edad : ['', [Validators.required,Validators.pattern("^[0-9]+$"), Validators.minLength(1), Validators.maxLength(2),]],
    sexo : ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$")]],
    telefono : ['', [Validators.required,Validators.pattern("^[0-9]+$"), Validators.minLength(9), Validators.maxLength(9),]],
    fecha : ['', [Validators.required]],
    hora : ['', [Validators.required]],
    })

    this.setEditar();
  }

  guardarEditarCita(){
    if (this.id === null) {
      this.guardar();
    }else{
      this.editar(this.id)
    }
  }

  guardar(){
    if(this.form.valid) {


    this.citaService.save(this.form.value).then(rsp => {
      Swal.fire({
        icon: 'success',
        title: 'exito',
        text: 'la cita fue guardada exitosamente',

      })
      this.router.navigate(['dental/citas']);

    })
  }else{
    this.form.markAllAsTouched();
  }

  }

  editar(id: string){
    if(this.form.valid === true){
      this.citaService.actualizarCita(id, this.form.value).then(() =>{
        Swal.fire({
          icon: 'success',
          title: 'exito',
          text: 'la cita fue editada exitosamente',

        })
        this.router.navigate(['dental/citas']);
      }).catch(
        e => {
          Swal.fire({
            icon: 'error',
            title: 'fallo',
            text: `la cita no pudo ser registrada ${e.message}`,
          })
        }
      )

    }

  }

  setEditar(){

    if (this.id !== null) {
      this.titulo = 'Editar Cita'
      this.citaService.getCita(this.id).subscribe(data => {
        console.log(data.payload.data()['nombres']);
        this.form.setValue({
          // id:         data.payload.data()['id'],
          apellidos:  data.payload.data()['apellidos'],
          dni:        data.payload.data()['dni'],
          edad:       data.payload.data()['edad'],
          fecha:      data.payload.data()['fecha'],
          hora:       data.payload.data()['hora'],
          nombres:    data.payload.data()['nombres'],
          sexo:       data.payload.data()['sexo'],
          telefono:   data.payload.data()['telefono']
        })
      })
    }
  }

  keyup(){
    this.dni_search_fired = false;
  }

  search(dni: string, v: any){

    //cerificar que solo se presione una vez
    if (this.dni_search_fired == false) {
      this.dni_search_fired == true;
    }else{
      return;
    }

    //verificar errores
    if (v != null) {
      console.log("existen errores");
      return;
    }

    var promesa1 = this.citaService.search(dni).toPromise().then(data => {
      console.log("Promesa 01");
      //data.docs[0].data()
      return data;
    });

    var promesa3 = this.pacienteService.search(dni).toPromise().then(data => {
      console.log("Promesa 03");
      return data;
    });

    var promesa2 = promesa1.then(data => {
      console.log("promesa 02");
      if (data.docs.length > 0) {
        console.log("se encontro cita");
        this.setdata(data.docs[0].data());
        promesa2.then();
        console.log("aqui es uu");
        console.log(data);
      }else{
        console.log("Buscar en promesa 3");
        promesa3.then(data => {
          if (data.docs.length > 0) {
            console.log("se encontro un paciente");
            this.setdata2(data.docs[0].data());
          console.log("aqui es uu");
          console.log(data);
          }else{
            console.log("No se encontro ni en cita ni en paciente");

          }
        });
      }
    });

  }
  setdata(data){
    this.form.setValue({
      dni: data.dni,
    nombres: data.nombres ,
    apellidos : data.apellidos,
    sexo : data.sexo ,
    edad : data.edad ,
    telefono : data.telefono,
    fecha : '',
    hora: ''
    // console.log(data.nombres);
    // console.log(data.apellidos);
    // console.log(data.sexo);
    // console.log(data.edad);
    // console.log(data.telefono);
    })
  }

  setdata2(data){
    this.form.setValue({
      dni: data.dni,
    nombres: data.nombres ,
    apellidos : data.apellidos,
    sexo : data.sexo ,
    edad : data.edad ,
    telefono : data.telefono,
    fecha : '',
    hora: ''
    // console.log(data.nombres);
    // console.log(data.apellidos);
    // console.log(data.sexo);
    // console.log(data.edad);
    // console.log(data.telefono);
    })

  }

}
