import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmaciondialogo',
  templateUrl: './confirmaciondialogo.component.html',
  styleUrls: ['./confirmaciondialogo.component.css']
})
export class ConfirmaciondialogoComponent implements OnInit {


  mensaje: string;
  btn = 'aceptar';
  constructor(public dialogRef: MatDialogRef<ConfirmaciondialogoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.mensaje = data.mensaje;

     }

  ngOnInit(): void {
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
}
