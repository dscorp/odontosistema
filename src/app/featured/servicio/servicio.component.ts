import { Component, OnInit } from '@angular/core';
import { Servicio } from 'src/app/shared/model/servicio';


@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.css']
})
export class ServicioComponent implements OnInit {

  servicios : Array<Servicio> = [];

  constructor() {
    this.servicios.push({ nombre : "servicio_A" , precio : 5 });
    this.servicios.push({ nombre : "servicio_B" , precio : 10 });
    this.servicios.push({ nombre : "servicio_C" , precio : 5 });
    this.servicios.push({ nombre : "servicio_D" , precio : 5 });
  }

  ngOnInit(): void {
  }

}
