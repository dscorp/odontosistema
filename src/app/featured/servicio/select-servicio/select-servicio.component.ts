import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-servicio',
  templateUrl: './select-servicio.component.html',
  styleUrls: ['./select-servicio.component.css']
})
export class SelectServicioComponent implements OnInit {

  servicios : Array<String> = [];

  constructor() {
    this.servicios.push("servicio_A");
    this.servicios.push("servicio_B");
    this.servicios.push("servicio_C");
    this.servicios.push("servicio_D");
  }

  ngOnInit(): void {
  }

}
