
import { RouterModule, Routes } from "@angular/router";
import { AntecedenteComponent } from '../featured/antecedente/antecedente.component';
import { ListarcitaComponent } from '../featured/cita/listarcita/listarcita.component';
import { RegistrarcitaComponent } from '../featured/cita/registrarcita/registrarcita.component';
import { ConsultaComponent } from "../featured/consulta/consulta.component";
import { HistorialReporteComponent } from '../featured/consulta/historial-reporte/historial-reporte.component';
import { ListarConsultaComponent } from '../featured/contenedorconsulta/listar-consulta/listar-consulta.component';
import { ExamenclinicoComponent } from '../featured/examenclinico/examenclinico.component';
import { ObjectOdontogramaComponent } from '../featured/odontograma/object-odontograma/object-odontograma.component';
import { OdontogramaComponent } from "../featured/odontograma/odontograma.component";
import { ListPacienteComponent } from '../featured/paciente/list-paciente/list-paciente.component';
import { PacienteComponent } from "../featured/paciente/paciente.component";
import { ListarPagosPacienteComponent } from '../featured/pagos/listar-pagos-paciente/listar-pagos-paciente.component';
import { PagoConsultaComponent } from '../featured/pagos/pagoconsulta/pagoconsulta.component';
import { RecetaComponent } from '../featured/receta/receta.component';
import { ServicioComponent } from "../featured/servicio/servicio.component";
import { TestComponent } from "../featured/test/test.component";
import { CrearTratamientoComponent } from '../featured/tratamiento/crear-tratamiento/crear-tratamiento.component';
import { ListarTratamientoComponent } from '../featured/tratamiento/listar-tratamiento/listar-tratamiento.component';
import { TratamientoComponent } from "../featured/tratamiento/tratamiento.component";
import { VerTratamientoComponent } from '../featured/tratamiento/ver-tratamiento/ver-tratamiento.component';
import { VercontenedorconsultaComponent } from '../featured/contenedorconsulta/vercontenedorconsulta/vercontenedorconsulta.component';
import { EnfermedadesactualesComponent } from './../featured/consulta/enfermedadesactuales/enfermedadesactuales.component';
import { ContenedorconsultaComponent } from './../featured/contenedorconsulta/contenedorconsulta.component';
import { PagoplantratamientoComponent } from './../featured/pagos/pagoplantratamiento/pagoplantratamiento.component';
import { GuardListarTratamiento } from './../featured/tratamiento/listar-tratamiento/guard-listar-tratamiento.guard';
import { BodyComponent } from "./components/body/body.component";
import { VerdatosconsultaComponent } from "../featured/consulta/verdatosconsulta/verdatosconsulta.component";

const coreRoutes :Routes= [
  {
      path:'',
      component: ListarcitaComponent
  },


  {
      path: 'antecedente',
      component : AntecedenteComponent
  },

  {
      path: 'paciente',
      component: PacienteComponent
  },

  {
    path : 'consulta',
    component: ConsultaComponent
  },

  {
    path: 'test',
    component: TestComponent
  },

  {
    path: 'odontograma/object',
    component: ObjectOdontogramaComponent
  },

  {
    path: 'odontograma/:id',
    component: OdontogramaComponent
  },

  {
    path: 'receta/:id',
    component: RecetaComponent
  },

  {
    path: 'tratamiento', component: TratamientoComponent
  },

  { path: 'tratamiento/crear/:id', component: CrearTratamientoComponent },
  { path: 'tratamiento/addsession/:id/:idtratamiento', component: CrearTratamientoComponent },

  {
    path : 'servicio',
    component : ServicioComponent
  },
  {
    path : 'citas',
    component : ListarcitaComponent,
    // children: [
    //   {
    //     path: 'editarcita/:id', component:RegistrarcitaComponent
    //   }
    // ]
  },
  {
    path : 'editarcita/:id',
    component : RegistrarcitaComponent
  },
  {
    path : 'nuevacita',
    component : RegistrarcitaComponent
  },

  {
    path: 'pagoconsulta',
    component:PagoConsultaComponent
  }
  ,{
    path: 'pagoplantratamiento',
    component:PagoplantratamientoComponent,
    canActivate:[GuardListarTratamiento]
  },
  {
    path: 'listapagospaciente',
    component:ListarPagosPacienteComponent,

  },
  {
    path: 'enfermedadesactuales/:id',
    component: EnfermedadesactualesComponent
  }
  ,
  {
    path: 'contenedorconsulta/:id/:nombre',
    component: ContenedorconsultaComponent

  },
  {
    path: 'examenclinico/:id',
    component: ExamenclinicoComponent
  },
  {
    path: 'consultas',
    component: ListarConsultaComponent
  },
  {
    path: 'pacientesMotivo',
    component: ListPacienteComponent
  },
  {
    path: 'listadoTratamientos',
    component: ListarTratamientoComponent
  },

  {
    path: 'tratamiento/:id',
    component: VerTratamientoComponent
  },
  {
    path: 'historialPaciente',
    component: HistorialReporteComponent
  },
  {
    path: 'consulta/:id',
    component: VercontenedorconsultaComponent
  },
  {
    path: 'datosconsulta/:id',
    component: VerdatosconsultaComponent
  },
]


export const CORE_ROUTES = RouterModule.forChild(coreRoutes);
