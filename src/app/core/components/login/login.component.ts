import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
   password:string;

  constructor(private auth:AuthService,
              private router:Router) {
    if(this.auth.leerUser()!=null)
    {

      this.router.navigate(['dental'])

    }
   }



  ngOnInit(): void {


  }

  onSubmit(form:NgForm)
  {

    this.auth.signInWithEmail(this.email,this.password)
  }

}
