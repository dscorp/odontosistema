import { Cita } from '../../../shared/model/cita';
import { CitasService } from './../../../shared/services/citas.service';
import { PacienteService } from './../../../shared/services/paciente.service';
import { AuthService } from './../../services/auth.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  @Input() opts: NavbarOpts;

  url: string;
  constructor(public auth: AuthService,
    private pacienteService: PacienteService,
    private citaService:CitasService) { }

  ngOnInit(): void {


    // console.log('antes de guardar');
    // this.guardarPaciente();
    // this.citaService.save(this.citaService.cita);

    // this.citaService.read().subscribe(data=>{
    //   console.log('citas');



    //   var lists = data.map(item => {
    //     return {
    //       id: item.payload.doc.id,
    //       ...item.payload.doc.data() as any
    //     } as Cita
    //   })
    //   console.log(lists);


    // })


  }




  toggleSidenav() {
    this.opts.sidenav.toggle(true, "program");
  }

  guardarPaciente() {
    this.pacienteService.save(this.pacienteService.paciente);
  }






}
















export interface NavbarOpts {
  /**
   * The title of the solution
   */
  title: string;
  /**
   * The link of the StackOverflow question
   */
  soLink: string;
  /**
   * Whether the navbar should have an elevation (z3)
   */
  hasEl?: boolean;
  /**
   * Sets the colour of the toolbar (Default is "primary")
   * NOTE: Since "none" is an invalid value, it won't give any colour to the toolbar
   */
  color?: "primary" | "accent" | "warn" | "none";
  /**
   * The sidenav to pass in
   */
  sidenav?: MatSidenav;
}
