import { MatSidenav } from '@angular/material/sidenav';
import { NavbarOpts } from './../toolbar/toolbar.component';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  opts: NavbarOpts;
  @ViewChild('sidenav') sidenav: MatSidenav;
  constructor() { }

  ngOnInit(): void {


  }

ngAfterContentChecked(): void {
    this.opts = {
      // Replace with the title of the example
      title: "Template",
      // Replace with the actual StackOverflow qn url
      soLink: "https://example.com",
      // If the example contains a toolbar at the top, set this as false
      hasEl: true,
      color:'accent',
      sidenav: this.sidenav
    }
  }
}
