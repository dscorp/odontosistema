import { Router, Routes } from '@angular/router';
import { User } from '../../shared/model/user';

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import Swal from 'sweetalert2';
import { Route } from '@angular/compiler/src/core';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public singedUser = new User();

  constructor(public auth: AngularFireAuth, private routes:Router) {
    this.leerUser()
  }

  signInWithEmail(email, password) {
    this.leerUser()
    if(this.singedUser!=null && this.singedUser.uid!=="")
    {
      this.routes.navigate(['dental'])
      console.log('entro');
      return
    }

    Swal.fire('Cargando')
    Swal.showLoading()
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(result => {
            Swal.close();
          this.saveUserOnLocalStorage(result.user.uid, result.user.email);
          // result.user.tenantId should be ‘TENANT_PROJECT_ID’.
          this.routes.navigate(['dental']);

        }).catch(function (error) {
          console.log(error);
          Swal.close();
          Swal.fire({
            icon: 'error',
            title: 'Credenciales incorrectas',
            text: 'asegurese de ingresar sus credenciales correctamente.',

          })


        });


  }

  saveUserOnLocalStorage(uid: string, email: string) {
    localStorage.setItem('uid', uid)
    localStorage.setItem('email', email)
    this.singedUser=new User();
    this.singedUser.uid = localStorage.getItem('uid');
    this.singedUser.email = localStorage.getItem('email')
  }


  leerUser() {

    if (localStorage.getItem('uid')) {
      this.singedUser=new User();
      this.singedUser.uid = localStorage.getItem('uid');
      this.singedUser.email = localStorage.getItem('email')

    } else {
      this.singedUser = null;
    }

    return this.singedUser;

  }

  singOut()
  {
    localStorage.clear();
    this.routes.navigate(['']);
  }

  isAutenticated= ()=> this.singedUser!=null?true:false;


}
