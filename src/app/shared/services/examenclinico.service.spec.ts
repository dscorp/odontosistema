import { TestBed } from '@angular/core/testing';

import { ExamenclinicoService } from './examenclinico.service';

describe('ExamenclinicoService', () => {
  let service: ExamenclinicoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExamenclinicoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
