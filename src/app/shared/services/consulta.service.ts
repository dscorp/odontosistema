import { ExamenClinico } from './../model/examenclinico';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { Paciente } from '../model/paciente';
import { PacienteService } from './paciente.service';
import { Receta } from '../model/receta';
import { Diente } from '../model/diente';
import { Consulta } from '../model/consulta';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ConsultaService {
  selectedConsulta: Consulta;
  consultaDesdeFirebase:Consulta;

  update(idConsulta,data)
  {
    return this.firestore.doc("consultas/"+idConsulta).update(data)
  }

  setReceta(id: string, listaDeRecetas:Receta[]) {
    return this.firestore.collection("consultas").doc(id).update({"listaDeRecetas":listaDeRecetas});
  }

  setodontograma(id: string, listDeOdontogramas: Diente[]) {
    return this.firestore.collection("consultas").doc(id).update({"listDeOdontogramas":listDeOdontogramas});
  }

  setExamenClinico(id: any, examenclinico: any) {
    return this.firestore.collection("consultas").doc(id).update({"examenClinico":examenclinico});
  }

  setEnfermedaesActuales(id: any, enfermedades: any) {
    return this.firestore.collection("consultas").doc(id).update({"listaDeEnfermedades":enfermedades});
  }

  public consulta = new Consulta();
  ref = this.firestore.collection('consultas');

  constructor(
    private firestore: AngularFirestore,
    private pacienteService: PacienteService
  ) {}

  // paginarsiguientes(ultimoDocumento, registrosAMostrar) {
  //   const query = this.ref.ref
  //     .orderBy('fecha', "asc")
  //     .startAfter(ultimoDocumento);
  //   return query.limit(registrosAMostrar).get();
  // }



  paginarsiguientes(ultimoDocumento, registrosAMostrar) {
    if(ultimoDocumento==null)
    {
      const query = this.firestore.collection('consultas', ref => ref.orderBy('fecha','desc').startAfter('').limit(registrosAMostrar))

      return query.get();
    }
    else{
      const query = this.firestore.collection('consultas', ref => ref.orderBy('fecha','desc').startAfter(ultimoDocumento).limit(registrosAMostrar))

      return query.get();
    }

      }


  save(consulta: Consulta, paciente: Paciente, id_consulta:any, paciente_nuevo : Boolean) {

    consulta.dni = paciente.dni;
    consulta.telefono = paciente.telefono;
    consulta.nombres = paciente.nombres;
    consulta.apellidos = paciente.apellidos;
    consulta.edad = paciente.edad;

    if(paciente_nuevo){
      this.pacienteService.save2(paciente,consulta.id_paciente);
    }

    return this.ref.doc(id_consulta).set(JSON.parse(JSON.stringify(consulta)));
  }

  read() {
    return this.ref.snapshotChanges();
  }

  getById(id: string) {
    return this.firestore.doc('consultas/' + id).valueChanges();
  }

  search(dni: string) {
    return this.firestore
      .collection('consultas', (ref) =>
        ref.where('dni', '==', dni).orderBy('fecha', 'desc')
      )
      .get();
  }

  searchMotivo(motivo: string, fechaInicial, fechafinal) {
    console.log(motivo);
    return this.firestore
      .collection('tratamientos', (ref) =>
        ref
          .where('fecha', '>=', fechaInicial)
          .where('fecha', '<=', fechafinal)
          .where('servicios', 'array-contains', motivo)
      )
      .get();
  }

  getExamenClinicio(id: string): Observable<any> {
    return this.ref.doc(id).snapshotChanges();
  }

  getReceta(id: string): Observable<any> {
    return this.ref.doc(id).snapshotChanges();
  }




  }


