import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  public paciente = {
    "nombres": "Sergio Cesar",
    "apellidos": "Carrillo Diestra",
    "dni": "48271836",
    "edad": 27,
    "sexo": "masculino",
    "lugar_nacimiento": "Santa Rosa",
    "fecha_nacimiento": "04/12/1993",
    "direccion": "mz k lt 2 irrigacion santa rosa sayan",
    "procedencia": "Santa ROsa",
    "ocupacion": "Explotado",
    "viajes": "huacho, mi casa, huacho, mi casa",
    "telefono": "978548754",
    "telefono_referencia": "954845475",
    "referencia": "Dennilson Munos",
    "antecedentes": {

      "personal": [
        "Covid-19",
        "Migrana"
      ],
      "familiar": [
        "ojismo",
        "lenguismo"
      ]
    }
  }

  constructor(private firestore: AngularFirestore) {
  }

  save2(paciente: any, id:string) {
    return this.firestore.collection("pacientes").doc(id).set(JSON.parse(JSON.stringify(paciente)));    
  }

  save(paciente: any) {
    return this.firestore.collection("pacientes").add(JSON.parse(JSON.stringify(paciente)));    
  }

  search(dni : string){
    return this.firestore.collection('pacientes', ref => ref.where('dni', '==', dni)).get();
  }

  getById(id: string) {
    return this.firestore.doc('pacientes/'+id).valueChanges();
  }

  get(){
    return this.firestore.collection('pacientes').get();
  }

}
