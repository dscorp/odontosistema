import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Tratamiento } from 'src/app/shared/model/tratamiento';
import { Sesion } from '../model/sesion';

@Injectable({
  providedIn: 'root'
})
export class TratamientoService {
  update(id: any, data: { montoAbonado: any; }) {
    return this.firestore.doc("tratamientos/"+id).update(data)
  }



  tratamientoSelected:Tratamiento

  // ref: AngularFirestoreCollection<any> = this.firestore.collection("tratamientos");
  ref = this.firestore.collection("tratamientos")

  constructor(private firestore: AngularFirestore) { }

  paginarsiguientes(ultimoDocumento, registrosAMostrar){
    const query = this.ref.ref.orderBy('id_consulta').startAfter(ultimoDocumento);
    return query.limit(registrosAMostrar).get();
  }

  save(tratamiento:any){
    return this.firestore.collection("tratamientos").add(JSON.parse(JSON.stringify(tratamiento)));
  }

  read() {
    return this.ref.snapshotChanges();
  }

  search(dni : string){
    return this.firestore.collection('tratamientos', ref => ref.where('dniPaciente', '==', dni)).get();
  }

  actualizarMontosGeneralesTratamientoPagos(tratamiento:any){
    return this.firestore.collection("tratamientos").doc(tratamiento.id).update({montoAbonado:tratamiento.montoAbonado});
  }

  getById(id: string):Observable<any>{
    return this.firestore.collection('tratamientos').doc(id).snapshotChanges();
  }

  getById2(id: string):Observable<any>{
    return this.firestore.doc('tratamientos/'+id).valueChanges();
  }

  updateSessiones(id:string , costoTotal :number ,lstsessiones : Sesion[] ){
    this.firestore.collection("tratamientos").doc(id).update({"costoTotal":costoTotal});
    return this.firestore.collection("tratamientos").doc(id).update({"sessiones":JSON.parse(JSON.stringify(lstsessiones))});
  }

  // getById(id: string): Observable <any> {
  //   return this.ref.doc(id).valueChanges();
  // }
}
