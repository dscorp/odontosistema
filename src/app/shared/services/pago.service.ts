import { Pago } from './../model/pago';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PagoService {
  constructor(private firestore: AngularFirestore) {}

  save(pago: Pago) {
    var pago = { ...pago };

    console.log(pago);
    return this.firestore.collection<any>('pagos').add(pago);
  }

  getByidConsulta(idConsulta: string) {
    return this.firestore.collection('pagos', (ref) =>
      ref.where('idConsulta', '==', idConsulta)
    );
  }

  getByidPlanTratamiento(idPlanTratamiento: string) {
    return this.firestore.collection('pagos', (ref) =>
      ref.where('idPlanTratamiento', '==', idPlanTratamiento)
    );
  }

  getByDniPaciente(dni) {
    return this.firestore.collection('pagos', (ref) =>
      ref.where('dniPaciente', '==', dni).orderBy('fecha','desc')
    );
  }
}
