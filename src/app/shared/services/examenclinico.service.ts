import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamenclinicoService {

  ref = this.firestore.collection("examenClinico");

  constructor(private firestore: AngularFirestore) { }

  save(examenClinico:any)
  {
      return this.ref.add(examenClinico);
  }
  getExamenClinicio(id: string): Observable <any> {
    return this.ref.doc(id).snapshotChanges();
  }
}
 