import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';

@Injectable({
  providedIn: 'root'
})
export class CitasService {

  ref: AngularFirestoreCollection<any> = this.firestore.collection("citas");
  // cita = {
  //   "fecha":"04/12/2020",
  //   "hora": "12:30",
  //   "nombres": "Dennilson",
  //   "apellidos": "Munos Rojas",
  //   "dni":"48757485",
  //   "edad": 23,
  //   "sexo":"trans",
  //   "telefono":"984658458"
  // }


  constructor(private firestore: AngularFirestore) {



  }



  paginarsiguientes(ultimoDocumento,registrosAMostrar) {
    const query = this.ref.ref.orderBy('fecha').orderBy('hora', 'desc').startAfter(ultimoDocumento);

    return query.limit(registrosAMostrar).get();

  }



  save(cita: any) {
    return this.ref.add(cita);
  }

  read() {
    // snapshot te devuelve el metada a diferencia del valuechanges
    return this.ref.snapshotChanges();
  }

  delete(citaId: string) {
    this.ref.doc(citaId).delete();
  }

  search(dni : string){
    return this.firestore.collection('citas', ref => ref.where('dni', '==', dni)).get();
  }

  getCita(id: string): Observable<any> {
    return this.ref.doc(id).snapshotChanges();
  }

  actualizarCita(id: string, data: any): Promise<any> {
    return this.ref.doc(id).update(data);
  }

}
