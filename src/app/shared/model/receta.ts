export class Receta {
    id: string;
    nombre_generico_medicamento: string;
    dosis: string;
    cuidado: string;
    via_administracion: string;
    tiempo: string;
    medidas_higienicas:string;
}