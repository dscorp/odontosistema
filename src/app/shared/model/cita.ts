export class Cita {
  id: string;
  apellidos: string;
  dni: string;
  edad: string;
  fecha: string;
  hora: string;
  nombres: string;
  sexo: string
  telefono: string
}
