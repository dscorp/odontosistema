export interface Diente{
    numero : number;
    color? : string;
    observacion? : String;
    especificacion? : String;
    cuadrante_1?: string;
    cuadrante_2?: string;
    cuadrante_3?: string;
    cuadrante_4?: string;
    cuadrante_5?: string;
  }
  