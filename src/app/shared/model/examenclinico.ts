export class ExamenClinico{
    pulso: number;
    temperatura: string;
    frecuenciaCardiaca: string;
    frecuenciaRespiratoria: string;
    examenClinicoGeneral: string;
    examenClinicoOdontologico: string;

}
