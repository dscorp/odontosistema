import { Timestamp } from 'rxjs';
import { Sesion } from './sesion';

export class Tratamiento {
  costoTotal: number;
  montoAbonado: number;
  dniPaciente: string;
  nombreCompleto:string;
  nombreTratamiento:string;
  sessiones = new Array<Sesion>();
  fecha: number = new Date().getTime();
  id_consulta: string;
  id: string;
  servicios=new Array<String>()
  telefonoPaciente:String
}
