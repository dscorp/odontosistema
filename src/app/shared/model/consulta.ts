import { ExamenClinico } from './examenclinico';
import { EnfermedadActual } from "./enfermedadactualmodel";
import { Receta } from "./receta";
import { Diente } from './diente';

export class Consulta{
  id:string
  id_paciente:string
  motivo:string;
  nombres:string;
  apellidos:string;
  edad:number;
  dni:string;
  telefono:string
  nombreprofesional:string;
  diagnosticopresun:string;
  diagnosticodefini:string;
  fecha = new Date().getTime();
  hora:string;
  listaDeEnfermedades = new Array<EnfermedadActual>();
  listDeOdontogramas = new Array<Diente>();
  examenClinico = new ExamenClinico();
  listaDeRecetas = new Array<Receta>();
  isWithTratamiento=false;
}
