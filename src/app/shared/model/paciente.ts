import { Antecedente } from "./antecedente";

export class Paciente{
    id:string;
    nombres:string;
    dni:string;
    edad:number;
    apellidos:string;
    sexo:string;
    lugar_nacimiento:string;
    fecha_nacimiento:string;
    direccion:string;
    procedencia:string;
    ocupacion:string;
    viajes:string;
    telefono:string;
    telefono_referencia:string;
    referencia:string;
    listadeAntecedentes = new Array<Antecedente>();
}