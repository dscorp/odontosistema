export class Pago {
  monto: number;
  concepto: string="";
  fecha: number;
  idPlanTratamiento:string="";
  idConsulta:string="";
  isPagoDeTratamiento:boolean=false;
  dniPaciente:string;
}
