import { Paciente } from './../model/paciente';
export class Utils{

 static covertirdatafirestoreToArray(data: any) {
    const dataAsArray = new Array<any>();
    data.docs.map((doc) => {
      dataAsArray.push({
        ...doc.data(),
        id: doc.id,
      });
    });
    return dataAsArray;
  }


  static servicios = [
    {text: 'Consulta Exam. OX'},
    {text: 'Analg. Simple'},
    {text: 'Analg. Compuesto'},
    {text: 'Analg. Modificado'},
    {text: 'Resina. Mod'},
    {text: 'Resina. Mod'},
    {text: 'Inscrustaciones/PKT'},
    {text: 'Exod. Simple/Ret'},
    {text: 'Cirugia'},
    {text: 'Apiceptomia'},
    {text: 'Endodocnia Ant'},
    {text: 'Endodocnia Post'},
    {text: 'Momificacion pulpar'},
    {text: 'Destartraj/ Profilaxis'},
    {text: 'Protesis Parcial Remo'},
    {text: 'Protesis Acrilico Total'},
    {text: 'Coronas'},
    {text: 'Otros'}
  ]

}
