import { ElementRef, HostListener, Output } from '@angular/core';
import { Directive, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appScrollable]'
})
export class ScrollableDirective {

  @Output() scrollPosition = new EventEmitter();


  constructor(public el: ElementRef) {
    console.log('diretiva aplicada');
    console.log(el);
  }


  @HostListener('mouseenter') onMouseEnter() {
    console.log('mouse dentro');
  }




  @HostListener('scroll', ['$event'])
  onScroll() {

    // console.log('scroll');
// console.log(this.el);
    // const top = event.target.scrollTop;
    const pos = this.el.nativeElement.scrollTop;

    const offsetheight = this.el.nativeElement.offsetHeight;
    const scrollheight = this.el.nativeElement.scrollHeight
    // console.log({ pos, offsetheight, scrollheight });
    // console.log(pos, scrollheight- offsetheight-1);

    if(pos> scrollheight- offsetheight-1)
    {
      this.scrollPosition.emit('bottom')
    }
    if(pos===0)
    {
      this.scrollPosition.emit('top')
    }

  }



}
