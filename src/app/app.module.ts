import { ExamenclinicoComponent } from './featured/examenclinico/examenclinico.component';
import { MatMenuModule, MAT_MENU_SCROLL_STRATEGY } from '@angular/material/menu';
import { APP_ROUTES } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MatInputModule } from '@angular/material/input';
import { environment } from 'src/environments/environment';
import { MatCardModule } from '@angular/material/card';
import { LoginComponent } from './core/components/login/login.component';
import {  MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {MatRadioModule} from '@angular/material/radio';
import { OdontogramaComponent } from './featured/odontograma/odontograma.component';
import { OverlayModule } from '@angular/cdk/overlay';
import {MatGridListModule} from '@angular/material/grid-list';
import { ConfdienteComponent } from './featured/odontograma/confdiente/confdiente.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
import { TratamientoComponent } from './featured/tratamiento/tratamiento.component';
import { ServicioComponent } from './featured/servicio/servicio.component';
import { PacienteComponent } from './featured/paciente/paciente.component';
import { ConsultaComponent } from './featured/consulta/consulta.component';
import { TestComponent } from './featured/test/test.component';
import { MatListModule } from '@angular/material/list';
import { PagoConsultaComponent } from './featured/pagos/pagoconsulta/pagoconsulta.component';
import { PagoplantratamientoComponent } from './featured/pagos/pagoplantratamiento/pagoplantratamiento.component';
import { RegistrarcitaComponent } from './featured/cita/registrarcita/registrarcita.component';
import { ListarcitaComponent } from './featured/cita/listarcita/listarcita.component';
import { ConfirmaciondialogoComponent } from './featured/cita/confirmaciondialogo/confirmaciondialogo.component';
import { EnfermedadesactualesComponent } from './featured/consulta/enfermedadesactuales/enfermedadesactuales.component';
import { ContenedorconsultaComponent } from './featured/contenedorconsulta/contenedorconsulta.component';
import { ListarConsultaComponent } from './featured/contenedorconsulta/listar-consulta/listar-consulta.component';
import { RecetaComponent } from './featured/receta/receta.component';
import { ScrollableDirective } from './shared/directives/scrollable.directive';
import { AntecedenteComponent } from './featured/antecedente/antecedente.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { CrearTratamientoComponent } from './featured/tratamiento/crear-tratamiento/crear-tratamiento.component';
import { SelectServicioComponent } from './featured/servicio/select-servicio/select-servicio.component';
import { ObjectOdontogramaComponent } from './featured/odontograma/object-odontograma/object-odontograma.component';
import { DialognuevopagoComponent } from './featured/pagos/dialognuevopago/dialognuevopago.component';
import localePe from '@angular/common/locales/es-PE';
import { registerLocaleData, DatePipe } from '@angular/common';
import { ListPacienteComponent } from './featured/paciente/list-paciente/list-paciente.component';
import { ListarTratamientoComponent } from './featured/tratamiento/listar-tratamiento/listar-tratamiento.component';
import { VerTratamientoComponent } from './featured/tratamiento/ver-tratamiento/ver-tratamiento.component';
import { HistorialReporteComponent } from './featured/consulta/historial-reporte/historial-reporte.component';
import { VercontenedorconsultaComponent } from './featured/contenedorconsulta/vercontenedorconsulta/vercontenedorconsulta.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { ListarPagosPacienteComponent } from './featured/pagos/listar-pagos-paciente/listar-pagos-paciente.component';
import {MatTabsModule} from '@angular/material/tabs';
import { MatSortModule } from '@angular/material/sort';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatNativeDateModule } from '@angular/material/core';
import { VerdatosconsultaComponent } from './featured/consulta/verdatosconsulta/verdatosconsulta.component';
registerLocaleData(localePe,'es-PE')

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OdontogramaComponent,
    ConfdienteComponent,
    TratamientoComponent,
    ServicioComponent,
    PacienteComponent,
    ConsultaComponent,
    TestComponent,
    PagoConsultaComponent,
    PagoplantratamientoComponent,
    RegistrarcitaComponent,
    ListarcitaComponent,
    ConfirmaciondialogoComponent,
    EnfermedadesactualesComponent,
    ContenedorconsultaComponent,
    ExamenclinicoComponent,
    ListarConsultaComponent,
    RecetaComponent,
    ScrollableDirective,
    AntecedenteComponent,
    CrearTratamientoComponent,
    SelectServicioComponent,
    ObjectOdontogramaComponent,
    DialognuevopagoComponent,
    ListPacienteComponent,
    ListarTratamientoComponent,
    VerTratamientoComponent,
    HistorialReporteComponent,
    VercontenedorconsultaComponent,
    ListarPagosPacienteComponent,
    VerdatosconsultaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    OverlayModule,
    //SI NO SE IMPORTA MATMENUMODULE EN EL MODULO ROOT NO FUNCIONA EN LOS MODULOS HIJOS
    MatMenuModule,
    MatGridListModule,
    MatDialogModule,
    MatPaginatorModule,
    MatIconModule,
    MatListModule,
    MatStepperModule,
    MatSnackBarModule,
    MatRadioModule,
    MatSelectModule,
    MatProgressSpinnerModule,

    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatNativeDateModule ,
    APP_ROUTES,

    SweetAlert2Module.forRoot(),

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-PE' },
    DatePipe,
    MatDatepickerModule,
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfdienteComponent]
})
export class AppModule {


}
